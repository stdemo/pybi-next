from . import styles


def textBase():
    return styles.fontSize() + styles.lineHeight()
