from pathlib import Path

pj_name = "pybi"

cur_dir = Path(__file__).parent.absolute()
root = cur_dir.parent

assets = root / "dist" / "assets"
public_dir = root / "public"

echarts_js = public_dir / "echarts.min.js"
js = next(assets.glob("*.js"))

html = cur_dir / "template.html"


def js_into_template(echarts: Path, js: Path, html: Path):
    content = html.read_text("utf8")
    content = content.replace("[js]", js.read_text("utf8"))
    content = content.replace("[echarts]", echarts.read_text("utf8"))

    return content


dest = root.parent / pj_name / "template" / "index.html"

data = js_into_template(echarts_js, js, html)
dest.write_text(data, "utf8")

# with open(dest, 'w', encoding='utf8') as f:
#     f.write(data)
# dest.write_text(data, 'utf8')

print(f"done for bulid index.html. path:{dest}")
