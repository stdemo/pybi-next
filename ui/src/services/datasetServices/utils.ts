import { TCpId } from "@/models/types";
import { readonly, ref, Ref } from "vue";
import { getBlacklistFilterFn } from "./filterBlacklist";
import { TComponentServices } from "@/services/componentServices";


export function* iterFilterExpr(cp2FilterMap: Map<TCpId, Ref<string>>, requestorId: string, exceptIds: TCpId[], cpServices: TComponentServices) {

    const exceptIdsSet = new Set<TCpId>([...exceptIds, requestorId])

    const blacklistFn = getBlacklistFilterFn(requestorId, cpServices)

    for (const [id, expression] of cp2FilterMap.entries()) {

        if (!exceptIdsSet.has(id) && !blacklistFn(id)) {
            if (expression.value !== '') {
                yield readonly(expression)
            }

        }

    }

}