import { describe, it, expect } from "vitest";
import { getServices } from ".";
import { getServices as getSqlServices } from "@/services/sqlAnalyzeServices";
import { getServices as getComponentServices } from "@/services/componentServices";
import { TDbServices } from "@/services/dbServices";

import { ComponentTag } from "@/models/component";
import { ref } from "vue";

function createApp() {
    const dataSources = [] as any[]
    const dataViews = [] as any[]
    const children = [] as any[]


    function addCp(id: string, tag: ComponentTag, sql: string,) {
        const cp = {
            id, tag, updateInfos: [] as any
        }
        children.push(cp)

        function addUpdateInfos(table: string, field: string) {
            cp.updateInfos.push({
                table, field
            })

            return self
        }

        const self = {
            addUpdateInfos
        }

        return self
    }

    function addSource(name: string) {
        dataSources.push({ name })
    }

    function addView(name: string, sql: string, excludeLinkages: string[] = []) {
        dataViews.push({ name, type: 'sql', sql, excludeLinkages })
    }

    function getApp() {
        return {
            children, dataSources, dataViews
        } as any
    }

    return {
        getApp,
        addView,
        addSource,
        addCp,
    }

}


const mockDbServices: TDbServices = {
    isReady: ref(true),
    uploadDbFile: (file) => { },
    queryAll: (code: string): any => { },
    query2tempory: (tableName: string, query: string) => { return tableName },
    runOnTransaction: <TReturn>(handler: () => TReturn, type?: "COMMIT" | "ROLLBACK") => { return handler() },
    getTableFields: (table: string) => { return [] },
}



describe('filter to single dataset testing', () => {

    const builder = createApp()
    builder.addSource('ds_2')
    builder.addCp('3', ComponentTag.Slicer, 'select distinct 区域 from ds_2')
        .addUpdateInfos('ds_2', '区域')
    builder.addCp('4', ComponentTag.Slicer, 'select distinct 名称 from ds_2')
        .addUpdateInfos('ds_2', '名称')
    builder.addCp('5', ComponentTag.Slicer, 'select distinct A from ds_2')
        .addUpdateInfos('ds_2', 'A')

    const app = builder.getApp()

    const service = getServices(app, { sqlAnalyze: getSqlServices(), component: getComponentServices(app) })

    it('without filter', () => {
        const act = service.createSql('ds_2', '3')
        expect(act.value).toBe('select * from ds_2')
    })

    it('has filter', () => {
        const act = service.createSql('ds_2', '3')
        service.addFilter('4', 'ds_2', `名称="a"`)
        expect(act.value).toBe(`select * from ds_2 where 名称="a"`)
    })


    it('filters', () => {
        const act = service.createSql('ds_2', '3')
        service.addFilter('4', 'ds_2', `名称="a"`)
        service.addFilter('5', 'ds_2', `A="x"`)
        expect(act.value).toBe(`select * from ds_2 where 名称="a" and A="x"`)
    })

})


describe('filter to multiple dataset testing', () => {
    const builder = createApp()
    builder.addSource('ds_2')
    builder.addSource('ds_3')


    builder.addCp('3', ComponentTag.Slicer, 'select distinct 区域 from ds_2')
        .addUpdateInfos('ds_2', '区域')
    builder.addCp('4', ComponentTag.Slicer, 'select distinct 名称 from ds_3')
        .addUpdateInfos('ds_3', '名称')
        .addUpdateInfos('ds_2', '名称')


    const app = builder.getApp()

    const service = getServices(app, { sqlAnalyze: getSqlServices(), component: getComponentServices(app), db: mockDbServices })

    it('has filter', () => {
        const act = service.createSql('ds_2', '3')
        service.addFilter('4', 'ds_3', `名称="a"`)
        service.addFilter('4', 'ds_2', `名称="a"`)
        expect(act.value).toBe(`select * from ds_2 where 名称="a"`)
    })

})




describe('filter to multiple dataview testing', () => {
    const builder = createApp()
    builder.addSource('ds_1')
    builder.addView('dv_1', 'select a,b,c from ds_1')


    builder.addCp('3', ComponentTag.Slicer, 'select distinct 区域 from dv_1')
        .addUpdateInfos('dv_1', '区域')
    builder.addCp('4', ComponentTag.Slicer, 'select distinct 名称 from ds_1')
        .addUpdateInfos('ds_1', '名称')


    const app = builder.getApp()

    const service = getServices(app, { sqlAnalyze: getSqlServices(), component: getComponentServices(app), db: mockDbServices })

    it('dv link to ds', () => {
        const act = service.createSql('dv_1', '3')
        service.addFilter('4', 'ds_1', `名称="a"`)
        expect(act.value).toBe(`select a,b,c from (select * from ds_1 where 名称="a")`)
    })

})


describe('dataview cancel ds link testing', () => {
    const builder = createApp()
    builder.addSource('ds_1')
    builder.addView('view1', 'select a,b,c from ds_1', ['ds_1'])
    builder.addView('view2', 'select a,b,c from view1')

    builder.addCp('3', ComponentTag.Slicer, 'select distinct 区域 from view1')
        .addUpdateInfos('view1', '区域')
    builder.addCp('4', ComponentTag.Slicer, 'select distinct 名称 from ds_1')
        .addUpdateInfos('ds_1', '名称')


    const app = builder.getApp()

    const service = getServices(app, { sqlAnalyze: getSqlServices(), component: getComponentServices(app), db: mockDbServices })

    it('test', () => {
        const act = service.createSql('view1', '3')
        service.addFilter('4', 'ds_1', `名称="a"`)
        expect(act.value).toBe(`select a,b,c from ds_1`)
    })

    it('multiple view', () => {
        const act = service.createSql('view2', '3')
        service.addFilter('4', 'ds_1', `名称="a"`)
        expect(act.value).toBe(`select a,b,c from (select a,b,c from ds_1)`)
    })

})



describe('chart update filter testing', () => {
    const builder = createApp()
    builder.addSource('ds_1')
    builder.addView('view1', 'select a,b,c from ds_1')

    builder.addCp('3', ComponentTag.Slicer, 'select distinct 区域 from view1')
        .addUpdateInfos('view1', '区域')
    builder.addCp('4', ComponentTag.EChart, 'select distinct 名称 from ds_1')
        .addUpdateInfos('ds_1', '名称')


    const app = builder.getApp()

    const service = getServices(app, { sqlAnalyze: getSqlServices(), component: getComponentServices(app), db: mockDbServices })

    it('slicer should not be updated', () => {
        const act = service.createSql('view1', '3')
        service.addFilter('4', 'ds_1', `名称="a"`)
        expect(act.value).toBe(`select a,b,c from (select * from ds_1)`)
    })



})




describe('chart update testing2', () => {
    const builder = createApp()
    builder.addSource('ds_2')
    builder.addView('dv_3', 'select 区域 as name,avg(销售额) as value from ds_2 group by 区域')

    builder.addCp('3', ComponentTag.Slicer, 'select distinct name from dv_3')
        .addUpdateInfos('dv_3', 'name')
    builder.addCp('4', ComponentTag.EChart, 'select name,value from dv_3')


    const app = builder.getApp()

    const service = getServices(app, { sqlAnalyze: getSqlServices(), component: getComponentServices(app), db: mockDbServices })

    it('test', () => {
        const act = service.createSql('dv_3', '4')
        service.addFilter('3', 'dv_3', `name="a"`)

        const replaceDs = 'select 区域 as name,avg(销售额) as value from (select * from ds_2) group by 区域'
        const withFilters = `select * from (${replaceDs}) where name="a"`

        expect(act.value).toBe(withFilters)
    })



})