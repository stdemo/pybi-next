import { App } from "@/models/app";
import { getServices as getSqlServices } from "./sqlAnalyzeServices";
import { getServices as getComponentServices } from "./componentServices";
import { getServices as getDatasetServices } from "./datasetServices";
import { getServices as getDbServices } from "./dbServices";
import wasm_base64 from "@/assets/wasm_base64.json";

export function initServices(app: App) {
    const dbServices = getDbServices(app, wasm_base64)

    const cpServices = getComponentServices(app)
    const sqlServices = getSqlServices()
    const datasetServices = getDatasetServices(app, {
        sqlAnalyze: sqlServices,
        component: cpServices,
        db: dbServices,
    })


    return {
        cpServices,
        sqlServices,
        datasetServices,
        dbServices,
    }

}